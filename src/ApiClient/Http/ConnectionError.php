<?php

namespace ExperienceBank\Sdk\ApiClient\Http;

use ExperienceBank\Sdk\ApiClient\Exceptions\RuntimeException;

class ConnectionError extends RuntimeException
{
}
