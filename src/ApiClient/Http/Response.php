<?php

namespace ExperienceBank\Sdk\ApiClient\Http;

use ExperienceBank\Sdk\ApiClient\RequestError;
use ExperienceBank\Sdk\ApiClient\Exceptions\OutOfBoundsException;
use ExperienceBank\Sdk\ApiClient\Exceptions\RuntimeException;
use Psr\Http\Message\ResponseInterface as Psr7Response;

final class Response extends \GuzzleHttp\Psr7\Response
{
    /** @var array|null */
    private $data;

    /** @var \DateTimeImmutable|null */
    private $cachedSince;

    /**
     * @param Psr7Response $response
     *
     * @return static
     */
    public static function fromPsr7(Psr7Response $response)
    {
        return new static(
            $response->getStatusCode(),
            $response->getHeaders(),
            $response->getBody()
        );
    }

    /**
     * @param \DateTimeImmutable $cachedSince
     */
    public function setCachedSince(\DateTimeImmutable $cachedSince)
    {
        $this->cachedSince = $cachedSince;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getCachedSince()
    {
        return $this->cachedSince;
    }

    /**
     * @throws ConnectionError
     * @throws OutOfBoundsException
     * @throws RequestError
     * @throws RuntimeException
     */
    public function assertSuccessful()
    {
        $statusCode = $this->getStatusCode();

        if ($statusCode >= 200 && $statusCode < 300) {
            if (!$this->hasValue('error')) {
                return;
            }

            throw RequestError::create(
                $this->getValue('error.message'),
                $this->getValue('error.code'),
                $statusCode,
                (array) $this->getValueWithDefault('error.data', [])
            );
        }

        throw new ConnectionError(
            'JSON-RPC request failed. Got status code '.$statusCode,
            $statusCode
        );
    }

    /**
     * @return array
     *
     * @throws RuntimeException
     */
    public function getValues()
    {
        if ($this->data === null) {
            $body = $this->getBody()->getContents();

            if ($body === '') {
                $data = [];
            } else {
                $data = json_decode($body, true);
                if (!is_array($data)) {
                    throw new RuntimeException('Failed to JSON decode server response: '.json_last_error_msg().'. Response body: '.$body);
                }
            }

            $this->data = $data;
        }

        return $this->data;
    }

    /**
     * @param string $key
     * @param string $separator
     *
     * @return mixed
     * @throws OutOfBoundsException
     * @throws RuntimeException
     */
    public function getValue($key, $separator = '.')
    {
        return $this->getValueRecursively(explode($separator, $key), $this->getValues());
    }

    /**
     * @param string $key
     * @param mixed $default
     * @param string $separator
     *
     * @return mixed
     * @throws RuntimeException
     */
    public function getValueWithDefault($key, $default, $separator = '.')
    {
        try {
            return $this->getValue($key, $separator);
        } catch (OutOfBoundsException $e) {
            return $default;
        }
    }

    /**
     * @param string $key
     * @param string $separator
     *
     * @return bool
     * @throws RuntimeException
     */
    public function hasValue($key, $separator = '.')
    {
        try {
            $this->getValue($key, $separator);

            return true;
        } catch (OutOfBoundsException $e) {
            return false;
        }
    }

    /**
     * @param array $keys
     * @param array $data
     *
     * @return mixed
     *
     * @throws OutOfBoundsException
     */
    private function getValueRecursively(array $keys, array $data)
    {
        $key = array_shift($keys);

        if (array_key_exists($key, $data)) {
            if (count($keys) === 0) {
                return $data[$key];
            }

            return $this->getValueRecursively($keys, $data[$key]);
        }

        throw new OutOfBoundsException("Could not find key '$key' in data: ".print_r($data, true));
    }
}
