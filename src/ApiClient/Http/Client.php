<?php

namespace ExperienceBank\Sdk\ApiClient\Http;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

interface Client
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function request(Request $request);
}
