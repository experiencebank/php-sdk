<?php

namespace ExperienceBank\Sdk\ApiClient\Http;

use Base64Url\Base64Url;
use ExperienceBank\Sdk\ApiClient\Credentials;
use Psr\Log\LoggerAwareTrait;

final class Request extends \GuzzleHttp\Psr7\Request
{
    use LoggerAwareTrait;

    /**
     * @param Credentials $credentials
     *
     * @return Request
     */
    public function withAuthorizationHeader(Credentials $credentials)
    {
        $signature = $this->createSignature($credentials);

        $header = sprintf(
            'Basic %s',
            base64_encode(sprintf(
                '%s:%s',
                $credentials->getPublicKey(),
                $signature
            ))
        );

        return $this->withHeader('Authorization', $header);
    }

    /**
     * @param Credentials $credentials
     *
     * @return string
     */
    private function createSignature(Credentials $credentials)
    {
        $body = $this->getBody();
        $body->rewind();

        $data = Base64Url::encode($body->getContents());
        return hash_hmac('sha256', $data, $credentials->getSecretKey());
    }
}
