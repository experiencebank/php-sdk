<?php

namespace ExperienceBank\Sdk\ApiClient;

use Doctrine\Common\Cache\Cache;
use ExperienceBank\Sdk\ApiClient\Exceptions\OutOfBoundsException;

final class CredentialsStorage
{
    /** @var Cache */
    private $store;

    /**
     * @param Cache $store
     */
    public function __construct(Cache $store)
    {
        $this->store = $store;
    }

    /**
     * @param string      $key
     * @param Credentials $credentials
     */
    public function store($key, Credentials $credentials)
    {
        $this->store->save($key, $credentials, 0);
    }

    /**
     * @param string $key
     *
     * @return Credentials
     *
     * @throws OutOfBoundsException
     */
    public function find($key)
    {
        $credentials = $this->store->fetch($key);
        if (!$credentials) {
            throw new OutOfBoundsException("No credentials for key '{$key}' available.");
        }

        return $credentials;
    }
}
