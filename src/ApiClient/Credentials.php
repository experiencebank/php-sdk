<?php

namespace ExperienceBank\Sdk\ApiClient;

final class Credentials
{
    /** @var string */
    private $publicKey = '';

    /** @var string */
    private $secretKey = '';

    /**
     * @param string $publicKey
     * @param string $secretKey
     */
    public function __construct($publicKey, $secretKey)
    {
        $this->publicKey = $publicKey;
        $this->secretKey = $secretKey;
    }

    /**
     * @return string
     */
    public function getPublicKey()
    {
        return $this->publicKey;
    }

    /**
     * @return string
     */
    public function getSecretKey()
    {
        return $this->secretKey;
    }
}
