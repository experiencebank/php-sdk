<?php

namespace ExperienceBank\Sdk\ApiClient\Exceptions;

class OutOfBoundsException extends Exception
{
}
