<?php

namespace ExperienceBank\Sdk\ApiClient\Exceptions;

class InvalidArgumentException extends Exception
{
}
