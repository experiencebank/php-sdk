<?php

namespace ExperienceBank\Sdk\ApiClient\Exceptions;

class RuntimeException extends Exception
{
}
