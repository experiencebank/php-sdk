<?php

namespace ExperienceBank\Sdk\ApiClient\Methods\Supplier;

use ExperienceBank\Sdk\ApiClient\Client;

class GenerateAutoLoginUrl
{
    const METHOD_NAME = 'supplier.generateAutoLoginUrl';

    /** @var Client */
    private $client;
    private $supplierId;
    private $email;

    public function __construct(Client $client, $supplierId, $email)
    {
        $this->client = $client;
        $this->supplierId = $supplierId;
        $this->email = $email;
    }

    public function forDashboard()
    {
        return $this->request('dashboard');
    }

    public function forMappingPage($mappingId)
    {
        return $this->request('mapping', $mappingId);
    }

    private function request($targetPage, $mappingId = null)
    {
        return $this->client->request(self::METHOD_NAME, [
            'supplierId' => $this->supplierId,
            'email' => $this->email,
            'redirectUrl' => [
                'targetPage' => $targetPage,
                'data' => [
                    'mappingId' => $mappingId
                ]
            ]
        ]);
    }
}
