<?php

namespace ExperienceBank\Sdk\ApiClient\Methods\Supplier;

class Query
{
    /**
     * @var array
     */
    private $supplierIds = [];
    /**
     * @var string|null
     */
    private $cursor;


    /**
     * @param array $supplierIds
     * @param string|null $cursor
     */
    public function __construct(array $supplierIds = [], $cursor = null)
    {
        $this->supplierIds = $supplierIds;
        $this->cursor = $cursor;
    }

    /**
     * @param $cursor
     * @return Query
     */
    public function withCursor($cursor)
    {
        $query = clone $this;
        $query->cursor = $cursor;
        return $query;
    }

    /**
     * @return array
     */
    public function generate()
    {
        return [
            'supplierIds' => (array)$this->supplierIds,
            'cursor' => $this->cursor
        ];
    }
}
