<?php

namespace ExperienceBank\Sdk\ApiClient\Methods;

use ExperienceBank\Sdk\ApiClient\Http\Response;
use ExperienceBank\Sdk\ApiClient\Methods\Availability\Query;

final class Availability extends MethodsCollection
{

    /**
     * @param array $params
     *
     * @return Response
     */
    public function updated(array $params)
    {
        return $this->request('availability.updated', $params);
    }

    /**
     * @param Query $query
     * @return Response
     */
    public function find(Query $query)
    {
        return $this->request('availability.find', [
            'query' => $query->generate()
        ]);
    }
}
