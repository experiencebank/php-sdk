<?php

namespace ExperienceBank\Sdk\ApiClient\Methods;

use ExperienceBank\Sdk\ApiClient\Http\Response;
use ExperienceBank\Sdk\ApiClient\Methods\Supplier\GenerateAutoLoginUrl;
use ExperienceBank\Sdk\ApiClient\Methods\Supplier\Query;

final class Supplier extends MethodsCollection
{

    /**
     * @param array $params
     *
     * @return Response
     * @throws \ExperienceBank\Sdk\ApiClient\RequestError
     */
    public function create(array $params)
    {
        return $this->request('supplier.create', $params);
    }

    /**
     * @param array $params
     *
     * @return Response
     * @throws \ExperienceBank\Sdk\ApiClient\RequestError
     */
    public function update(array $params)
    {
        return $this->request('supplier.update', $params);
    }

    /**
     * @param $supplierId
     * @return Response
     * @throws \ExperienceBank\Sdk\ApiClient\RequestError
     */
    public function enable($supplierId)
    {
        return $this->request('supplier.enable', [
            'supplierId' => $supplierId
        ]);
    }

    /**
     * @param $supplierId
     * @return Response
     * @throws \ExperienceBank\Sdk\ApiClient\RequestError
     */
    public function disable($supplierId)
    {
        return $this->request('supplier.disable', [
            'supplierId' => $supplierId
        ]);
    }

    /**
     * @param $supplierId
     * @param $email
     * @return GenerateAutoLoginUrl
     */
    public function generateAutoLoginUrl($supplierId, $email)
    {
        return new GenerateAutoLoginUrl($this->client, $supplierId, $email);
    }

    /**
     * @param Query $query
     * @return Response
     */
    public function find(Query $query)
    {
        return $this->request('supplier.find', [
            'query' => $query->generate()
        ]);
    }
}
