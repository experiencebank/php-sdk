<?php

namespace ExperienceBank\Sdk\ApiClient\Methods;

use ExperienceBank\Sdk\ApiClient\Http\Response;
use ExperienceBank\Sdk\ApiClient\Methods\Activity\Query;

final class Activity extends MethodsCollection
{

    /**
     * @param array $params
     *
     * @return Response
     */
    public function updated(array $params)
    {
        return $this->request('activity.updated', $params);
    }

    /**
     * @param Query $query
     * @return Response
     */
    public function find(Query $query)
    {
        return $this->request('activity.find', [
            'query' => $query->generate()
        ]);
    }
}
