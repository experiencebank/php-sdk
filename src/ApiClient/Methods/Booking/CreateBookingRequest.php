<?php

namespace ExperienceBank\Sdk\ApiClient\Methods\Booking;

use Assert\Assert;

class CreateBookingRequest
{
    private $supplierId;
    private $bookingItems;
    private $holdDurationSeconds;
    private $language;

    public function __construct($supplierId, $holdDurationSeconds, array $bookingItems = [], string $language = '')
    {
        Assert::that($supplierId)->notEmpty('Supplier Id cannot be empty');
        Assert::that($holdDurationSeconds)->greaterThan(0, 'Hold duration should be more than 0');
        $this->supplierId = $supplierId;
        $this->holdDurationSeconds = $holdDurationSeconds;
        $this->bookingItems = $bookingItems;
        $this->language = $language;
    }

    public function addBookingItem(BookingItem $bookingItem)
    {
        $this->bookingItems[] = $bookingItem;
    }

    public function generate()
    {
        Assert::that($this->bookingItems)->notEmpty('Booking Items cannot be empty')->all()->isInstanceOf(BookingItem::class, 'Incorrect Booking Item');
        return [
            'supplierId' => $this->supplierId,
            'bookingItems' => $this->getBookingItemsAsArray(),
            'holdDurationSeconds' => (int)$this->holdDurationSeconds,
            'language' => $this->language
        ];
    }

    private function getBookingItemsAsArray()
    {
        $bookingItems = [];
        /** @var $bookingItem BookingItem*/
        foreach ($this->bookingItems as $bookingItem) {
            $bookingItems[] = $bookingItem->generate();
        }
        return $bookingItems;
    }
}
