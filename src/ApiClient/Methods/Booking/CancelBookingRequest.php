<?php

namespace ExperienceBank\Sdk\ApiClient\Methods\Booking;

use Assert\Assert;

class CancelBookingRequest
{
    private $bookingId;
    private $reason;
    private $note;

    public function __construct($bookingId, $reason = '', $note = '')
    {
        Assert::that($bookingId)->notEmpty('Booking Id cannot be empty');
        $this->bookingId = $bookingId;
        $this->reason = $reason;
        $this->note = $note;
    }

    public function generate()
    {
        return [
            'bookingId' => $this->bookingId,
            'reason' => $this->reason,
            'note' => $this->note
        ];
    }
}
