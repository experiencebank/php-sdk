<?php

namespace ExperienceBank\Sdk\ApiClient\Methods\Booking\Commit;

use Assert\Assert;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\Addon;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\BookingItem as BaseBookingItem;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\Guest;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\TicketCategory;

class BookingItem extends BaseBookingItem
{
    private $guests;
    private $addons;

    public function __construct(
        $activityId,
        $optionId,
        $date,
        array $ticketCategories = [],
        array $guests = [],
        array $addons = []
    ) {
        $this->guests = $guests;
        $this->addons = $addons;
        parent::__construct($activityId, $optionId, $date, $ticketCategories);
    }

    public function addGuest(Guest $guest)
    {
        $this->guests[] = $guest;
    }


    public function addAddon(Addon $addon)
    {
        $this->addons[] = $addon;
    }

    /**
     * @return array
     */
    public function generate()
    {
        Assert::that($this->ticketCategories)->notEmpty('Ticket categories cannot be empty')->all()->isInstanceOf(TicketCategory::class, 'Incorrect Ticket Category');
        return [
            'activityId' => $this->activityId,
            'optionId' => $this->optionId,
            'date' => $this->date,
            'ticketCategories' => $this->getTicketCategoriesAsArray(),
            'guests' => $this->getGuestsAsArray(),
            'addons' => $this->getAddonsAsArray()
        ];
    }

    /**
     * @return array
     */
    private function getAddonsAsArray()
    {
        $addons = [];

        /** @var $addon Addon*/
        foreach ($this->addons as $addon) {
            $addons[] = $addon->generate();
        }
        return $addons;
    }

    /**
     * @return array
     */
    private function getGuestsAsArray()
    {
        $guests = [];

        /** @var $guest Guest*/
        foreach ($this->guests as $guest) {
            $guests[] = $guest->generate();
        }
        return $guests;
    }
}
