<?php

namespace ExperienceBank\Sdk\ApiClient\Methods\Booking;

use Assert\Assert;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\Commit\BookingItem;

class CommitBookingRequest
{
    private $supplierId;
    private $bookingId;
    private $externalBookingReference;
    private $notes;
    private $payment;
    private $contact;
    private $bookingItems;
    private $language;

    public function __construct(
        $supplierId,
        $bookingId,
        $externalBookingReference,
        $notes,
        Payment $payment = null,
        Contact $contact = null,
        array $bookingItems = [],
        string $language = ''
    ) {
        Assert::that($supplierId)->notEmpty('Supplier Id cannot be empty');
        Assert::that($bookingId)->notEmpty('Booking Id cannot be empty');
        Assert::that($externalBookingReference)->notEmpty('External Booking Reference cannot be empty');
        $this->supplierId = $supplierId;
        $this->bookingId = $bookingId;
        $this->externalBookingReference = $externalBookingReference;
        $this->notes = $notes;
        $this->payment = $payment;
        $this->contact = $contact;
        $this->bookingItems = $bookingItems;
        $this->language = $language;
    }

    public function addBookingItem(BookingItem $bookingItem)
    {
        $this->bookingItems[] = $bookingItem;
    }

    public function generate()
    {
        Assert::that($this->bookingItems)->notEmpty('Booking Items cannot be empty')->all()->isInstanceOf(BookingItem::class, 'Incorrect Booking Item');
        return [
            'supplierId' => $this->supplierId,
            'bookingId' => $this->bookingId,
            'externalBookingReference' => $this->externalBookingReference,
            'notes' => $this->notes,
            'payment' => $this->payment instanceof Payment ? $this->payment->generate() : null,
            'contact' => $this->contact instanceof Contact ? $this->contact->generate() : null,
            'bookingItems' => $this->getBookingItemsAsArray(),
            'language' => $this->language
        ];
    }

    private function getBookingItemsAsArray()
    {
        $bookingItems = [];
        /** @var $bookingItem BookingItem*/
        foreach ($this->bookingItems as $bookingItem) {
            $bookingItems[] = $bookingItem->generate();
        }
        return $bookingItems;
    }
}
