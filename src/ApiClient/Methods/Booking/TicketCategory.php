<?php

namespace ExperienceBank\Sdk\ApiClient\Methods\Booking;

use Assert\Assert;

class TicketCategory
{
    private $externalTicketId;
    private $ticketCategory;
    private $count;

    public function __construct($ticketCategory, $count, $externalTicketId = '')
    {
        Assert::that($ticketCategory)->notEmpty('Ticket Category cannot be empty');
        Assert::that($count)->integer()->greaterThan(0, 'Count cannot be less than 1');
        $this->ticketCategory = $ticketCategory;
        $this->count = $count;
        $this->externalTicketId = $externalTicketId;
    }

    public function generate()
    {
        return [
            'ticketCategory' => $this->ticketCategory,
            'count' => (int)$this->count,
            'externalTicketId' => $this->externalTicketId
        ];
    }
}
