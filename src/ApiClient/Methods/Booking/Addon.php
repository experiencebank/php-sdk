<?php

namespace ExperienceBank\Sdk\ApiClient\Methods\Booking;

use Assert\Assert;

class Addon
{
    private $id;
    private $quantity;

    public function __construct($id, $quantity)
    {
        Assert::that($id)->notEmpty('Addon Id cannot be empty');
        Assert::that($quantity)->greaterThan(0, 'Addon Quantity cannot be less than 1');
        $this->id = $id;
        $this->quantity = $quantity;
    }

    public function generate()
    {
        return [
            'id' => $this->id,
            'quantity' => (int)$this->quantity,
        ];
    }
}
