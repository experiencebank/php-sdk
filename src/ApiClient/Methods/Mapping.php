<?php

namespace ExperienceBank\Sdk\ApiClient\Methods;

use ExperienceBank\Sdk\ApiClient\Http\Response;

final class Mapping extends MethodsCollection
{


    /**
     * @param string $supplierId
     * @param string $marketplaceId
     * @param string $partnerId
     * @return Response
     */
    public function enable($supplierId, $marketplaceId, $partnerId)
    {
        return $this->request('mapping.enable', [
            'supplierId' => $supplierId,
            'marketplaceId' => $marketplaceId,
            'partnerId' => $partnerId
        ]);
    }


    /**
     * @param $mappingId
     * @return Response
     */
    public function disable($mappingId)
    {
        return $this->request('mapping.disable', [
            'mappingId' => $mappingId
        ]);
    }
}
