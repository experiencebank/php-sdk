<?php

namespace ExperienceBank\Sdk\ApiClient\Methods\Activity;

use Assert\Assert;

class Query
{
    /**
     * @var string
     */
    private $supplierId;
    /**
     * @var array
     */
    private $activityIds = [];
    /**
     * @var array
     */
    private $categoryIds = [];
    /**
     * @var string|null
     */
    private $cursor;


    public function __construct($supplierId, array $activityIds = [], $cursor = null, array $categoryIds = [])
    {
        Assert::that($supplierId)->notEmpty($supplierId, 'Supplier Id cannot be empty');
        $this->supplierId = (string)$supplierId;
        $this->activityIds = $activityIds;
        $this->categoryIds = $categoryIds;
        $this->cursor = $cursor;
    }

    /**
     * @param $cursor
     * @return Query
     */
    public function withCursor($cursor)
    {
        $query = clone $this;
        $query->cursor = $cursor;
        return $query;
    }

    /**
     * @return array
     */
    public function generate()
    {
        return [
            'supplierId' => (string)$this->supplierId,
            'activityIds' => (array)$this->activityIds,
            'categoryIds' => (array)$this->categoryIds,
            'cursor' => $this->cursor
        ];
    }
}
