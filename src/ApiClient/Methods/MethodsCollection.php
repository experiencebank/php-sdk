<?php

namespace ExperienceBank\Sdk\ApiClient\Methods;

use ExperienceBank\Sdk\ApiClient\Client;
use ExperienceBank\Sdk\ApiClient\Http\Response;

abstract class MethodsCollection
{
    /** @var Client */
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $method
     * @param array $params
     *
     * @return Response
     * @throws \ExperienceBank\Sdk\ApiClient\RequestError
     */
    protected function request($method, array $params)
    {
        return $this->client->request($method, $params);
    }
}
