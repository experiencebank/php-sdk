<?php

namespace ExperienceBank\Sdk\ApiClient\Methods;

use ExperienceBank\Sdk\ApiClient\Http\Response;

final class Ticket extends MethodsCollection
{
    /**
     * @param array $params
     *
     * @return Response
     */
    public function affected(array $params)
    {
        return $this->request('ticket.affected', $params);
    }
}
