<?php

namespace ExperienceBank\Sdk\ApiClient\Methods\Availability;

use Assert\Assert;

class Query
{
    /**
     * @var string
     */
    private $supplierId;

    /**
     * @var string
     */
    private $activityId;

    /**
     * @var string
     */
    private $fromDate;

    /**
     * @var string
     */
    private $untilDate;

    /**
     * @var array
     */
    private $optionIds = [];

    /**
     * @var string|null
     */
    private $cursor;


    public function __construct($supplierId, $activityId, $fromDate, $untilDate, array $optionIds = [], $cursor = null)
    {
        Assert::that($supplierId)->notEmpty($supplierId, 'Supplier Id cannot be empty');
        Assert::that($activityId)->notEmpty($activityId, 'Activity Id cannot be empty');
        Assert::that($fromDate)->regex('/^\d{4}-\d{2}-\d{2}$/', 'Incorrect date format for fromDate');
        Assert::that($untilDate)->regex('/^\d{4}-\d{2}-\d{2}$/', 'Incorrect date format for untilDate');
        $this->supplierId = (string)$supplierId;
        $this->activityId = (string)$activityId;
        $this->fromDate = (string)$fromDate;
        $this->untilDate = (string)$untilDate;
        $this->optionIds = $optionIds;
        $this->cursor = $cursor;
    }

    /**
     * @param $cursor
     * @return Query
     */
    public function withCursor($cursor)
    {
        $query = clone $this;
        $query->cursor = $cursor;
        return $query;
    }

    /**
     * @return array
     */
    public function generate()
    {
        return [
            'supplierId' => (string)$this->supplierId,
            'activityId' => (string)$this->activityId,
            'fromDate' => (string)$this->fromDate,
            'untilDate' => (string)$this->untilDate,
            'optionIds' => (array)$this->optionIds,
            'cursor' => $this->cursor
        ];
    }
}
