<?php

namespace ExperienceBank\Sdk\ApiClient\Methods;

use ExperienceBank\Sdk\ApiClient\Http\Response;
use ExperienceBank\Sdk\ApiClient\Methods\Activity\Query;

final class Category extends MethodsCollection
{
    public function find(Query $query)
    {
        return $this->request('category.find', []);
    }
}
