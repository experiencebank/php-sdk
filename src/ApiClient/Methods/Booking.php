<?php

namespace ExperienceBank\Sdk\ApiClient\Methods;

use ExperienceBank\Sdk\ApiClient\Http\Response;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\CancelBookingRequest;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\CommitBookingRequest;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\CreateBookingRequest;

final class Booking extends MethodsCollection
{

    /**
     * @param string $bookingId
     *
     * @return Response
     */
    public function cancelled($bookingId)
    {
        return $this->request('booking.cancelled', [
            'bookingId' => $bookingId
        ]);
    }

    /**
     * @param CancelBookingRequest $request
     * @return Response
     */
    public function cancel(CancelBookingRequest $request)
    {
        return $this->request('booking.cancel', $request->generate());
    }

    public function create(CreateBookingRequest $request)
    {
        return $this->request('booking.create', $request->generate());
    }

    public function commit(CommitBookingRequest $request)
    {
        return $this->request('booking.commit', $request->generate());
    }
}
