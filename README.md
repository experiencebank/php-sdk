
# ExperienceBank PHP SDK

The ExperienceBank PHP SDK including an API client library

## Installation

    composer require experiencebank/php-sdk

## Configuration

### Simple client

    <?php
    use ExperienceBank\Sdk\ApiClient\Client;
    use ExperienceBank\Sdk\ApiClient\Credentials;
    
    $credentials = new Credentials(
        getenv('API_PUBLIC_KEY'),
        getenv('API_SECRET_KEY')
    );
    
    $apiClient = new Client($credentials);

### Client with Redis cache and Monolog logging

    <?php
    use Doctrine\Common\Cache\RedisCache;
    use Monolog\Handler\StreamHandler;
    use Monolog\Logger;
    use ExperienceBank\Sdk\ApiClient\Client;
    use ExperienceBank\Sdk\ApiClient\Credentials;
    
    $redis = new \Redis();
    $redis->pconnect('127.0.0.1', 6379);
    
    $cache = new RedisCache();
    $cache->setRedis($redis);
    
    $credentials = new Credentials(
        getenv('API_PUBLIC_KEY'),
        getenv('API_SECRET_KEY')
    );

    $logger = new Logger('api-client');
    $logger->pushHandler(new StreamHandler('api-client.log'));
    
    $apiClient = Client::newCachingClient($credentials, $cache, $logger);

## Usage

### Creating a supplier

    $response = $apiClient->supplier()->create([
        'name'=> 'Amazing Demo Activities',
        'partnerSupplierId' => '15873',
        'partner' => 'par_049a72c3-7a8d-48aa-94d1-0ba5a8e9e9f2',
        'contact' => [
            'name' => 'John Doe',
            'email' => 'another@example.com'
        ]
    ]);
    
    $supplierId = $response->getValue('result.supplierId'); 
    
The following methods are currently supported:

    $this->client->supplier()->create(array $params);
    $this->client->supplier()->update(array $params);
    $this->client->supplier()->enable($supplierId);
    $this->client->supplier()->disable($supplierId);
    $this->client->supplier()->find(Query $query);
    $this->client->supplier()->generateAutoLoginUrl($supplierId, $email)->forMapping($mappingId);
    $this->client->activity()->updated(array $params);
    $this->client->activity()->find(Query $query);
    $this->client->availability()->updated(array $params);
    $this->client->availability()->find(Query $query);
    $this->client->booking()->create(CreateBookingRequest $request);
    $this->client->booking()->commit(CommitBookingRequest $request);
    $this->client->booking()->cancel(CancelBookingRequest $request);
    $this->client->booking()->cancelled($bookingId);
    $this->client->ticket()->affected(array $params);
    $this->client->mapping()->enable($supplierId, $marketplaceId, $partnerId);
    $this->client->mapping()->disable($mappingId);

    
    
    
But you can also call other RPC methods using the `request()` method.

    $this->client->request(string $method, array $params);

## Contributions

All contributions are welcomed through pull requests.

Please run tests (`vendor/bin/phpunit`) and Coding Style fixer (`vendor/bin/php-cs-fixer fix src --rules=@Symfony`)
before submitting.

## License

MIT. See LICENSE file.
