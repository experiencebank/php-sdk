<?php

namespace ExperienceBank\Sdk\Tests\ApiClient\Methods\Activity;

use Assert\InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use ExperienceBank\Sdk\ApiClient\Methods\Activity\Query;

final class QueryTest extends TestCase
{
    /** @test */
    public function i_can_create_query()
    {
        $query = new Query('1', ['2', '3'], 'cur_2dskdf');
        $this->assertEquals([
            'supplierId' => '1',
            'activityIds' => ['2', '3'],
            'cursor' => 'cur_2dskdf'
        ], $query->generate());
    }

    /** @test */
    public function i_can_create_query_with_cursor()
    {
        $query = new Query('1', ['2', '3']);
        $query = $query->withCursor('iNCVnmI==');
        $this->assertEquals([
            'supplierId' => '1',
            'activityIds' => ['2', '3'],
            'cursor' => 'iNCVnmI=='
        ], $query->generate());
    }

    /** @test */
    public function i_can_create_query_with_empty_activities()
    {
        $query = new Query('1');
        $this->assertEquals([
            'supplierId' => '1',
            'activityIds' => [],
            'cursor' => null
        ], $query->generate());
    }

    /**
     * @test
     * @expectedException  InvalidArgumentException
     */
    public function i_cannot_create_query_with_empty_supplier_id()
    {
        new Query('', ['2', '3']);
    }
}
