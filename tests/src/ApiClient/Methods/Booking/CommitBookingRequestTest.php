<?php

namespace ExperienceBank\Sdk\Tests\ApiClient\Methods\Booking;

use PHPUnit\Framework\TestCase;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\AdditionalField;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\Addon;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\Commit\BookingItem;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\CommitBookingRequest;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\Contact;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\Guest;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\Payment;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\TicketCategory;

final class CommitBookingRequestTest extends TestCase
{
    /** @test */
    public function i_can_create_booking_request()
    {
        $bookingRequest = new CommitBookingRequest(
            'sup_1',
            'boo_1',
            'extRef_1',
            'test notes',
            new Payment('20.00', 'USD'),
            new Contact('John Doe', 'john.doe@example.com', '111111')
        );
        $bookingItem = new BookingItem('act_1', 'opt_1', '2018-11-16');
        $bookingItem->addTicketCategory(new TicketCategory('tic_1', 5, 'extTic_1'));
        $guest = new Guest('John', 'Doe', 'john.doe@example.com', '111111');
        $guest->addAddon(new Addon('add_1', 2));
        $guest->addAdditionalField(new AdditionalField('nationality', 'BY'));
        $bookingItem->addGuest($guest);
        $bookingItem->addAddon(new Addon('add_2', 4));
        $bookingRequest->addBookingItem($bookingItem);
        $this->assertEquals([
            'supplierId' => 'sup_1',
            'bookingId' => 'boo_1',
            'externalBookingReference' => 'extRef_1',
            'bookingItems' => [
                [
                    'activityId' => 'act_1',
                    'optionId' => 'opt_1',
                    'date' => '2018-11-16',
                    'ticketCategories' => [
                        [
                            'ticketCategory' => 'tic_1',
                            'count' => 5,
                            'externalTicketId' => 'extTic_1'
                        ]
                    ],
                    'guests' => [
                        [
                            'firstName' => 'John',
                            'lastName' => 'Doe',
                            'emailAddress' => 'john.doe@example.com',
                            'phoneNumber' => '111111',
                            'additionalFields' => [
                                [
                                    'key' => 'nationality',
                                    'value' => 'BY'
                                ]
                            ],
                            'addons' => [
                                [
                                    'id' => 'add_1',
                                    'quantity' => 2
                                ]
                            ]
                        ]
                    ],
                    'addons' => [
                        [
                            'id' => 'add_2',
                            'quantity' => 4
                        ]
                    ]
                ]
            ],
            'payment' => [
                'amount' => '20.00',
                'currency' => 'USD'
            ],
            'contact' => [
                'fullName' => 'John Doe',
                'email' => 'john.doe@example.com',
                'phoneNumber' => '111111'
            ],
            'notes' => 'test notes'
        ], $bookingRequest->generate());
    }

    /** @test */
    public function i_can_create_booking_request_without_payment_and_contact()
    {
        $bookingRequest = new CommitBookingRequest('sup_1', 'boo_1', 'extRef_1', 'test notes');
        $bookingItem = new BookingItem('act_1', 'opt_1', '2018-11-16');
        $bookingItem->addTicketCategory(new TicketCategory('tic_1', 5, 'extTic_1'));
        $guest = new Guest('John', 'Doe', 'john.doe@example.com', '111111');
        $bookingItem->addGuest($guest);
        $bookingRequest->addBookingItem($bookingItem);
        $this->assertEquals([
            'supplierId' => 'sup_1',
            'bookingId' => 'boo_1',
            'externalBookingReference' => 'extRef_1',
            'bookingItems' => [
                [
                    'activityId' => 'act_1',
                    'optionId' => 'opt_1',
                    'date' => '2018-11-16',
                    'ticketCategories' => [
                        [
                            'ticketCategory' => 'tic_1',
                            'count' => 5,
                            'externalTicketId' => 'extTic_1'
                        ]
                    ],
                    'guests' => [
                        [
                            'firstName' => 'John',
                            'lastName' => 'Doe',
                            'emailAddress' => 'john.doe@example.com',
                            'phoneNumber' => '111111',
                            'additionalFields' => [],
                            'addons' => []
                        ]
                    ],
                    'addons' => []
                ]
            ],
            'payment' => null,
            'contact' => null,
            'notes' => 'test notes'
        ], $bookingRequest->generate());
    }
}
