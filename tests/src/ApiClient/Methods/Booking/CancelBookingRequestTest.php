<?php

namespace ExperienceBank\Sdk\Tests\ApiClient\Methods\Booking;

use PHPUnit\Framework\TestCase;
use ExperienceBank\Sdk\ApiClient\Methods\Booking\CancelBookingRequest;

final class CancelBookingRequestTest extends TestCase
{
    /** @test */
    public function i_can_create_cancel_booking_request()
    {
        $request = new CancelBookingRequest('boo_1', 'reason', 'notes');
        $this->assertEquals([
            'bookingId' => 'boo_1',
            'reason' => 'reason',
            'note' => 'notes',
        ], $request->generate());
    }
}
