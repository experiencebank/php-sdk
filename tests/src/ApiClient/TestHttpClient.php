<?php

namespace ExperienceBank\Sdk\Tests\ApiClient;

use ExperienceBank\Sdk\ApiClient\Http\Client as HttpClient;
use GuzzleHttp\Utils;
use Psr\Http\Message\RequestInterface as Request;
use ExperienceBank\Sdk\ApiClient\Http\Response;

final class TestHttpClient implements HttpClient
{
    public function request(Request $request)
    {
        $body = $request->getBody();
        $body->rewind();
        $data = Utils::jsonDecode($body->getContents());

        switch ($data->method) {
            case 'supplier.create':
                return new Response(200, ['content-type' => 'application/json'], Utils::jsonEncode([
                    'jsonrpc' => '2.0',
                    'result' => [
                        'supplierId' => 'b0780dc7-d605-495f-9c89-4c4f10ab4170',
                    ],
                    'id' => $data->id,
                ]));
            case 'booking.create':
                return new Response(200, ['content-type' => 'application/json'], Utils::jsonEncode([
                    'jsonrpc' => '2.0',
                    'result' => [
                        'bookingId' => 'boo_b0780dc7-d605-495f-9c89-4c4f10ab4170',
                        'expiresAt' => 11234234
                    ],
                    'id' => $data->id,
                ]));

            default:
                throw new \LogicException('Not implemented');
        }
    }
}
